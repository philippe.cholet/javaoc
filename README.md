# javaoc

Ce dépôt contient des solutions écrites en Java à des puzzles d'[Advent of Code](https://adventofcode.com/).
Chaque solution est testée sur les exemples fournis et sur mes "puzzle inputs".

Mes solutions sont ici principalement adaptées de celles que j'ai précédemment écrites en Rust : https://github.com/Philippe-Cholet/rusty-aoc/.

J'utilise ici _VS Code_ comme IDE, _Maven_ pour gérer le projet et _Spring Boot_ pour faire une petite application web.
J'ai également découvert _SonarLint_ qui est un linter très utile.

Afin de respecter les droits d'auteur, mes "puzzle inputs" ne sont pas partagées publiquement.

## Routes

Il y a actuellement deux routes, retournant des données JSON:
- `/solve/all` résout les deux parties de tous les puzzles ayant une solution et une "puzzle input".
- `/solve` résout les deux parties d'un seul puzzle. Ceci nécessite deux paramètres `year` et `day` et une "puzzle input" comme données à envoyer, qui peut être différente de mes "puzzle inputs" stockées dans `./src/main/resources/inputs`.

Les deux commandes suivantes retournent les mêmes données JSON, excepté que les temps d'exécution en millisecondes peuvent différer.

```bash
curl "http://localhost:8090/solve/all" | jq -c ".[] | select(.year == 2022 and .day == 1)"
curl "http://localhost:8090/solve?year=2022&day=1" -H "Content-Type:text/plain" -X POST --data-binary "@./src/main/resources/inputs/2022/01.txt"
```

```json
{"year":2022,"day":1,"part1":{"result":"71124","ms":1},"part2":{"result":"204639","ms":4}}
```

## Gitlab CI

J'ai encrypté avec [`git-crypt`](https://github.com/AGWA/git-crypt) et partagé mes "puzzle inputs" pour les tester dans Github CI mais je ne suis apparemment pas le seul à avoir des soucis à installer cet outil dans la CI.
Je me contente donc de tester les exemples.
