package fr.choletp.javaoc.year2022;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.choletp.javaoc.InputLoader;

class Day2Test {
    @Test
    void example() {
        Day2 day = new Day2();
        String input = "A Y\nB X\nC Z\n";
        assertEquals("15", day.solve(false, input));
        assertEquals("12", day.solve(true, input));
    }

    @Test
    void input() {
        String input = InputLoader.loadInput(2022, 2);
        Day2 day = new Day2();
        assertEquals("11767", day.solve(false, input));
        assertEquals("13886", day.solve(true, input));
    }
}
