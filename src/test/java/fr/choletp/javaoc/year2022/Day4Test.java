package fr.choletp.javaoc.year2022;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.choletp.javaoc.InputLoader;

class Day4Test {
    @Test
    void example() {
        Day4 day = new Day4();
        String input = """
            2-4,6-8
            2-3,4-5
            5-7,7-9
            2-8,3-7
            6-6,4-6
            2-6,4-8
            """;
        assertEquals("2", day.solve(false, input));
        assertEquals("4", day.solve(true, input));
    }

    @Test
    void input() {
        String input = InputLoader.loadInput(2022, 4);
        Day4 day = new Day4();
        assertEquals("644", day.solve(false, input));
        assertEquals("926", day.solve(true, input));
    }
}
