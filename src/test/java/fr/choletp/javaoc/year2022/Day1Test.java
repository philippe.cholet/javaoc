package fr.choletp.javaoc.year2022;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.choletp.javaoc.InputLoader;

class Day1Test {
    @Test
    void example() {
        Day1 day = new Day1();
        String input = "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000\n";
        assertEquals("24000", day.solve(false, input));
        assertEquals("45000", day.solve(true, input));
    }

    @Test
    void input() {
        String input = InputLoader.loadInput(2022, 1);
        Day1 day = new Day1();
        assertEquals("71124", day.solve(false, input));
        assertEquals("204639", day.solve(true, input));
    }
}
