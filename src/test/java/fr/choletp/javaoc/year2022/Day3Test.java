package fr.choletp.javaoc.year2022;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.choletp.javaoc.InputLoader;

class Day3Test {
    @Test
    void example() {
        Day3 day = new Day3();
        String input = """
            vJrwpWtwJgWrhcsFMMfFFhFp
            jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
            PmmdzqPrVvPwwTWBwg
            wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
            ttgJtRGJQctTZtZT
            CrZsJsPPZsGzwwsLwLmpwMDw
            """;
        assertEquals("157", day.solve(false, input));
        assertEquals("70", day.solve(true, input));
    }

    @Test
    void input() {
        String input = InputLoader.loadInput(2022, 3);
        Day3 day = new Day3();
        assertEquals("7997", day.solve(false, input));
        assertEquals("2545", day.solve(true, input));
    }
}
