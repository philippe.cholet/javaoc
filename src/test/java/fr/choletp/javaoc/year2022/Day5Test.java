package fr.choletp.javaoc.year2022;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.choletp.javaoc.InputLoader;

class Day5Test {
    @Test
    void example() {
        Day5 day = new Day5();
        String input = 
            "    [D]    \n" +
            "[N] [C]    \n" +
            "[Z] [M] [P]\n" +
            " 1   2   3 \n" +
            "\n" +
            "move 1 from 2 to 1\n" +
            "move 3 from 1 to 3\n" +
            "move 2 from 2 to 1\n" +
            "move 1 from 1 to 2\n";
        assertEquals("CMZ", day.solve(false, input));
        assertEquals("MCD", day.solve(true, input));
    }

    @Test
    void input() {
        String input = InputLoader.loadInput(2022, 5);
        Day5 day = new Day5();
        assertEquals("LJSVLTWQM", day.solve(false, input));
        assertEquals("BRQWDBBJM", day.solve(true, input));
    }
}
