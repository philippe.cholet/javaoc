package fr.choletp.javaoc.year2022;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.choletp.javaoc.InputLoader;

class Day8Test {
    @Test
    void example() {
        Day8 day = new Day8();
        String input = """
            30373
            25512
            65332
            33549
            35390
            """;
        assertEquals("21", day.solve(false, input));
        assertEquals("8", day.solve(true, input));
    }

    @Test
    void input() {
        String input = InputLoader.loadInput(2022, 8);
        Day8 day = new Day8();
        assertEquals("1816", day.solve(false, input));
        assertEquals("383520", day.solve(true, input));
    }
}
