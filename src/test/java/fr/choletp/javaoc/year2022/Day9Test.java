package fr.choletp.javaoc.year2022;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.choletp.javaoc.InputLoader;

class Day9Test {
    @Test
    void example1() {
        Day9 day = new Day9();
        String input = "R 4\nU 4\nL 3\nD 1\nR 4\nD 1\nL 5\nR 2\n";
        assertEquals("13", day.solve(false, input));
        assertEquals("1", day.solve(true, input));
    }

    @Test
    void example2() {
        Day9 day = new Day9();
        String input = "R 5\nU 8\nL 8\nD 3\nR 17\nD 10\nL 25\nU 20\n";
        assertEquals("36", day.solve(true, input));
    }

    @Test
    void input() {
        String input = InputLoader.loadInput(2022, 9);
        Day9 day = new Day9();
        assertEquals("6037", day.solve(false, input));
        assertEquals("2485", day.solve(true, input));
    }
}
