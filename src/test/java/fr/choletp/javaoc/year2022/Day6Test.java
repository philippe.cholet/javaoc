package fr.choletp.javaoc.year2022;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import fr.choletp.javaoc.InputLoader;

class Day6Test {
    @ParameterizedTest
    @CsvSource({
        "mjqjpqmgbljsphdztnvjfqwrcgsmlb, 7, 19",
        "bvwbjplbgvbhsrlpgdmjqwftvncz, 5, 23",
        "nppdvjthqldpwncqszvftbrmjlhg, 6, 23",
        "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg, 10, 29",
        "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw, 11, 26",
    })
    void example(String input, String part1, String part2) {
        Day6 day = new Day6();
        assertEquals(part1, day.solve(false, input));
        assertEquals(part2, day.solve(true, input));
    }

    @Test
    void input() {
        String input = InputLoader.loadInput(2022, 6);
        Day6 day = new Day6();
        assertEquals("1929", day.solve(false, input));
        assertEquals("3298", day.solve(true, input));
    }
}
