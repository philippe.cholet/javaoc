package fr.choletp.javaoc.year2023;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.choletp.javaoc.InputLoader;

class Day1Test {
    @Test
    void example1() {
        Day1 day = new Day1();
        String input = """
            1abc2
            pqr3stu8vwx
            a1b2c3d4e5f
            treb7uchet
            """;
        assertEquals("142", day.solve(false, input));
    }

    @Test
    void example2() {
        Day1 day = new Day1();
        String input = """
            two1nine
            eightwothree
            abcone2threexyz
            xtwone3four
            4nineeightseven2
            zoneight234
            7pqrstsixteen
            """;
        assertEquals("281", day.solve(true, input));
    }

    @Test
    void input() {
        String input = InputLoader.loadInput(2023, 1);
        Day1 day = new Day1();
        assertEquals("54990", day.solve(false, input));
        assertEquals("54473", day.solve(true, input));
    }
}
