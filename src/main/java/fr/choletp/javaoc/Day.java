package fr.choletp.javaoc;

public interface Day {
    public int day();

    public String solve(boolean part2, String text);
}
