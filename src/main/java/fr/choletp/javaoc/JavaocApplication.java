package fr.choletp.javaoc;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.choletp.javaoc.year2022.Year2022;
import fr.choletp.javaoc.year2023.Year2023;

@SpringBootApplication
@RestController
public class JavaocApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaocApplication.class, args);
    }

    private Optional<ObjectNode> timeSolveOne(ObjectMapper mapper, int year, int day, Optional<String> optionalInput) {
        Year yearObject;
        switch (year) {
            case 2022: yearObject = new Year2022(); break;
            case 2023: yearObject = new Year2023(); break;
            default: return Optional.empty();
        }
        Optional<Day> optionalSolver = yearObject.getDays().stream().filter(d -> d.day() == day).findFirst();

        return optionalSolver.map(daySolver -> {
            String input = optionalInput.orElseGet(() -> InputLoader.loadInput(year, day));

            Instant start = Instant.now();
            String res1 = daySolver.solve(false, input);
            Instant finish = Instant.now();
            long timeElapsed1 = Duration.between(start, finish).toMillis();

            start = Instant.now();
            String res2 = daySolver.solve(true, input);
            finish = Instant.now();
            long timeElapsed2 = Duration.between(start, finish).toMillis();

            var json = mapper.createObjectNode();
            json.put("year", year);
            json.put("day", day);
            var p1 = json.putObject("part1");
            p1.put("result", res1);
            p1.put("ms", timeElapsed1);
            var p2 = json.putObject("part2");
            p2.put("result", res2);
            p2.put("ms", timeElapsed2);
            return json;
        });
    }

    @PostMapping("/solve")
    public ObjectNode solve(@RequestParam int year, @RequestParam int day,
            @RequestBody String input) {
        var mapper = new ObjectMapper();
        return timeSolveOne(mapper, year, day, Optional.of(input)).orElseGet(() -> {
            var obj = mapper.createObjectNode();
            obj.put("error", "No solver yet!");
            return obj;
        });
    }

    @GetMapping("/solve/all")
    public ArrayNode solveAll() {
        var mapper = new ObjectMapper();
        var json = mapper.createArrayNode();
        Year[] years = { new Year2022(), new Year2023() };
        for (Year year : years) {
            for (int day = 1; day <= 25; day++) {
                timeSolveOne(mapper, year.year(), day, Optional.empty()).ifPresent(json::add);
            }
        }
        return json;
    }

}
