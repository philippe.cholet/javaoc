package fr.choletp.javaoc.year2023;

import java.util.List;

import fr.choletp.javaoc.Day;
import fr.choletp.javaoc.Year;

public class Year2023 implements Year {
    public int year() {
        return 2023;
    }

    public List<Day> getDays() {
        return List.of(
            new Day1()
        );
    }
}
