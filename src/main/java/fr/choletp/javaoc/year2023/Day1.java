package fr.choletp.javaoc.year2023;

import java.util.List;
import java.util.Optional;

import fr.choletp.javaoc.Day;

class Day1 implements Day {
    public int day() {
        return 1;
    }

    private final String[] digits1 = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    private final String[] digits2 = {
        "1", "2", "3", "4", "5", "6", "7", "8", "9",
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    };

    private int processLine(String[] strDigits, String line) {
        Optional<List<Integer>> first = Optional.empty();
        Optional<List<Integer>> last = Optional.empty();
        int digit = 1;
        for (String s : strDigits) {
            int idx = line.indexOf(s);
            if (idx != -1 && (first.isEmpty() || idx < first.get().getFirst())) {
                // minimal index and associated digit
                first = Optional.of(List.of(idx, digit));
            }
            idx = line.lastIndexOf(s);
            if (idx != -1 && (last.isEmpty() || last.get().getFirst() < idx)) {
                // maximal lastIndex and associated digit
                last = Optional.of(List.of(idx, digit));
            }
            digit = digit == 9 ? 1 : digit + 1;
        }
        return 10 * first.get().getLast() + last.get().getLast();
    }

    public String solve(boolean part2, String input) {
        String[] strDigits = part2 ? digits2 : digits1;
        int trebuchet = input.lines().mapToInt(line -> processLine(strDigits, line)).sum();
        return String.valueOf(trebuchet);
    }
}
