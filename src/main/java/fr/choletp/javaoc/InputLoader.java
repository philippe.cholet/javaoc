package fr.choletp.javaoc;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;

public class InputLoader {
    private InputLoader() {
        throw new IllegalStateException("Utility class");
    }

    public static String loadInput(int year, int day) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        String path = String.format("inputs/%d/%02d.txt", year, day);
        Resource resource = resourceLoader.getResource(path);
        try (Reader reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.US_ASCII)) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
