package fr.choletp.javaoc;

import java.util.List;

public interface Year {
    public int year();

    public List<Day> getDays();
}
