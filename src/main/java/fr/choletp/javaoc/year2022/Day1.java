package fr.choletp.javaoc.year2022;

import java.util.Comparator;
import java.util.PriorityQueue;

import fr.choletp.javaoc.Day;

class Day1 implements Day {
    public int day() {
        return 1;
    }

    public String solve(boolean part2, String input) {
        String[] blocks = input.split("\n\n");
        PriorityQueue<Integer> bests = new PriorityQueue<>(Comparator.reverseOrder());
        for (int i = 0; i < blocks.length; i++) {
            bests.add(blocks[i].lines().mapToInt(Integer::parseInt).sum());
        }
        int num = 0;
        if (part2) {
            num += bests.remove() + bests.remove();
        }
        num += bests.peek();
        return String.valueOf(num);
    }
}
