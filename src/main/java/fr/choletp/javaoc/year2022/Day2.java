package fr.choletp.javaoc.year2022;

import fr.choletp.javaoc.Day;

class Day2 implements Day {
    public int day() {
        return 2;
    }

    public String solve(boolean part2, String input) {
        int total = input.lines().mapToInt(line -> {
            int elf = line.charAt(0) - 'A';
            int xyz = line.charAt(2) - 'X'; // Part1: me as player ; Part2: outcome
            if (!part2) {
                return fightOutcome(xyz, elf) * 3 + xyz + 1;
            } else {
                return xyz * 3 + shapeAgainst(xyz, elf) + 1;
            }
        }).sum();
        return String.valueOf(total);
    }

    private static int fightOutcome(int player1, int player2) {
        switch (player1) {
            case 0: switch (player2) {
                case 1: return 0;
                case 0: return 1;
                case 2: return 2;
                default: return invalidPlayer();
            }
            case 1: switch (player2) {
                case 2: return 0;
                case 1: return 1;
                case 0: return 2;
                default: return invalidPlayer();
            }
            case 2: switch (player2) {
                case 0: return 0;
                case 2: return 1;
                case 1: return 2;
                default: return invalidPlayer();
            }
            default: return invalidPlayer();
        }
    }

    private static int shapeAgainst(int outcome, int player) {
        switch (outcome) {
            case 0: switch (player) {
                case 1: return 0;
                case 2: return 1;
                case 0: return 2;
                default: return invalidPlayer();
            }
            case 1: switch (player) {
                case 0: return 0;
                case 1: return 1;
                case 2: return 2;
                default: return invalidPlayer();
            }
            case 2: switch (player) {
                case 2: return 0;
                case 0: return 1;
                case 1: return 2;
                default: return invalidPlayer();
            }
            default: throw new IllegalStateException("Valid outcomes: Lose(0)/Draw(1)/Win(2)");
        }
    }

    private static int invalidPlayer() {
        throw new IllegalStateException("Valid players: Rock(0)/Paper(1)/Scissors(2)");
    }
}
