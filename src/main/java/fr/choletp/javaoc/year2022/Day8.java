package fr.choletp.javaoc.year2022;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import fr.choletp.javaoc.Day;

class Day8 implements Day {
    public int day() {
        return 8;
    }

    private int solvePart1(List<int[]> grid) {
        int width = grid.get(0).length;
        int height = grid.size();
        int numberStarts = (height + width) * 2;
        ArrayList<int[]> data = new ArrayList<>(numberStarts);
        for (int col = 0; col < width; col++) {
            int[] elem0 = { 0, col, 1, 0, height };
            data.add(elem0);
            int[] elem1 = { height - 1, col, -1, 0, height };
            data.add(elem1);
        }
        for (int row = 0; row < height; row++) {
            int[] elem2 = { row, 0, 0, 1, width };
            data.add(elem2);
            int[] elem3 = { row, width - 1, 0, -1, width };
            data.add(elem3);
        }
        HashSet<Integer> visibleTreeLocations = new HashSet<>(numberStarts);
        for (int[] elem : data) {
            int row = elem[0];
            int col = elem[1];
            int dr = elem[2];
            int dc = elem[3];
            int times = elem[4];
            int hMax = -1;
            for (int i = 0; i < times; i++) {
                int h = grid.get(row)[col];
                if (h > hMax) {
                    visibleTreeLocations.add(row * width + col);
                    if (h == 9) {
                        break; // Nothing can be higher than 9.
                    }
                    hMax = h;
                }
                row += dr;
                col += dc;
            }
        }
        return visibleTreeLocations.size();
    }

    private int partialScenicScore(List<int[]> grid, int row, int col, int dr, int dc, int times) {
        int curHeight = grid.get(row)[col];
        int visible = 0;
        for (int i = 0; i < times; i++) {
            visible += 1;
            row += dr;
            col += dc;
            if (grid.get(row)[col] >= curHeight) {
                break;
            }
        }
        return visible;
    }

    public String solve(boolean part2, String input) {
        List<int[]> grid = input
            .lines()
            .map(line -> line.chars().map(ch -> ch - '0').toArray())
            .toList();
        if (!part2) {
            return String.valueOf(solvePart1(grid));
        }
        int width = grid.get(0).length;
        int height = grid.size();
        int maxScenicScore = 0;
        // Trees at the border have a scenic score of 0, ignore them.
        for (int row = 1; row < height - 1; row++) {
            for (int col = 1; col < width - 1; col++) {
                int scenicScore = partialScenicScore(grid, row, col, -1, 0, row)
                    * partialScenicScore(grid, row, col, 1, 0, height - 1 - row)
                    * partialScenicScore(grid, row, col, 0, -1, col)
                    * partialScenicScore(grid, row, col, 0, 1, width - 1 - col);
                maxScenicScore = Integer.max(scenicScore, maxScenicScore);
            }
        }
        return String.valueOf(maxScenicScore);
    }
}
