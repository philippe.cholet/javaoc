package fr.choletp.javaoc.year2022;

import java.util.List;

import fr.choletp.javaoc.Day;
import fr.choletp.javaoc.Year;

public class Year2022 implements Year {
    public int year() {
        return 2022;
    }

    public List<Day> getDays() {
        return List.of(
            new Day1(),
            new Day2(),
            new Day3(),
            new Day4(),
            new Day5(),
            new Day6(),
            new Day8(),
            new Day9()
        );
    }
}
