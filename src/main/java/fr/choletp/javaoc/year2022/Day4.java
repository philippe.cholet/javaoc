package fr.choletp.javaoc.year2022;

import fr.choletp.javaoc.Day;

class Day4 implements Day {
    public int day() {
        return 4;
    }

    public String solve(boolean part2, String input) {
        long count = input.lines().map(line -> {
            String[] split = line.split("[-,]");
            assert split.length == 4;
            int[] ns = { 0, 0, 0, 0 };
            int idx = 0;
            for (String s : split) {
                ns[idx] = Integer.parseInt(s);
                idx += 1;
            }
            assert ns[0] <= ns[1] && ns[2] <= ns[3];
            return ns;
        }).filter(ns -> {
            if (!part2) {
                return (ns[0] <= ns[2] && ns[3] <= ns[1]) || (ns[2] <= ns[0] && ns[1] <= ns[3]);
            } else {
                return !(ns[1] < ns[2] || ns[3] < ns[0]);
            }
        }).count();
        return String.valueOf(count);
    }
}
