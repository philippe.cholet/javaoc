package fr.choletp.javaoc.year2022;

import java.util.HashSet;

import fr.choletp.javaoc.Day;

class Day3 implements Day {
    public int day() {
        return 3;
    }

    public String solve(boolean part2, String input) {
        int count;
        if (!part2) {
            count = input.lines().mapToInt(line -> {
                int len = line.length();
                String s0 = line.substring(0, len / 2);
                String s1 = line.substring(len / 2, len);
                HashSet<Character> coll = uniqueCharacters(s0);
                coll.retainAll(uniqueCharacters(s1));
                return priority(coll);
            }).sum();
        } else {
            int idx = 0;
            HashSet<Character> coll = HashSet.newHashSet(0);
            count = 0;
            for (String line : input.lines().toList()) {
                if (idx == 0) {
                    coll = uniqueCharacters(line);
                } else {
                    coll.retainAll(uniqueCharacters(line));
                }
                idx += 1;
                if (idx == 3) {
                    idx = 0;
                    count += priority(coll);
                }
            }
            assert idx == 0;
        }
        return String.valueOf(count);
    }

    private HashSet<Character> uniqueCharacters(String s) {
        HashSet<Character> coll = HashSet.newHashSet(s.length());
        for (char ch : s.toCharArray()) {
            coll.add(ch);
        }
        return coll;
    }

    private static String azaz = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private int priority(HashSet<Character> chars) {
        assert chars.size() == 1;
        char ch = chars.iterator().next();
        return azaz.indexOf(ch) + 1;
    }
}
