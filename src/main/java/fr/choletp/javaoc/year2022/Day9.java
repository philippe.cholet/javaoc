package fr.choletp.javaoc.year2022;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;

import fr.choletp.javaoc.Day;

class Day9 implements Day {
    public int day() {
        return 9;
    }

    public String solve(boolean part2, String input) {
        int numberKnots = part2 ? 10 : 2;
        ArrayList<Point> rope = new ArrayList<>(numberKnots);
        Point origin = new Point(); // All knots start at the same point.
        for (int i = 0; i < numberKnots; i++) {
            rope.add(origin.getLocation());
        }
        HashSet<Point> tailPositions = new HashSet<>();
        tailPositions.add(origin);
        input.lines().forEach(line -> {
            Point move = charMove(line.charAt(0));
            assert line.charAt(1) == ' ';
            int numberMoves = Integer.parseInt(line.substring(2));
            for (int i = 0; i < numberMoves; i++) {
                rope.getFirst().translate(move.x, move.y);
                for (int j = 1; j < numberKnots; j++) {
                    followKnot(rope.get(j), rope.get(j - 1));
                }
                tailPositions.add(rope.getLast().getLocation());
            }
        });
        return String.valueOf(tailPositions.size());
    }

    private Point charMove(char move) {
        switch (move) {
            case 'D': return new Point(1, 0);
            case 'U': return new Point(-1, 0);
            case 'L': return new Point(0, -1);
            case 'R': return new Point(0, 1);
            default:
                assert false;
                return new Point();
        }
    }

    private void followKnot(Point knot, Point followed) {
        int dr = followed.x - knot.x;
        int dc = followed.y - knot.y;
        int dr2 = shorten(dr);
        int dc2 = shorten(dc);
        if ((dr != dr2) || (dc != dc2)) {
            knot.translate(dr2, dc2);
        }
    }

    private int shorten(int n) {
        assert -2 <= n && n <= 2;
        switch (n) {
            case -2, -1: return -1;
            case 0: return 0;
            case 1, 2: return 1;
            default: return 0; // Unreachable!
        }
    }
}
