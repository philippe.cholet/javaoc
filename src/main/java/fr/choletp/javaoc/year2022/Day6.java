package fr.choletp.javaoc.year2022;

import fr.choletp.javaoc.Day;

class Day6 implements Day {
    public int day() {
        return 6;
    }

    public String solve(boolean part2, String input) {
        int windowSize = part2 ? 14 : 4;
        // Chars are within a-z and 26 bits is enough.
        int[] bits = input.stripTrailing().chars().map(ch -> 1 << ch - 'a').toArray();
        for (int start = 0; start < bits.length - windowSize; start++) {
            int n = 0;
            boolean allUnique = true;
            for (int i = start; i < start + windowSize; i++) {
                if ((n & bits[i]) != 0) {
                    allUnique = false;
                    break;
                }
                n |= bits[i];
            }
            if (allUnique) {
                return String.valueOf(start + windowSize);
            }
        }
        return "";
    }
}
