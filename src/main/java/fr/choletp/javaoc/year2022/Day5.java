package fr.choletp.javaoc.year2022;

import java.util.Vector;

import fr.choletp.javaoc.Day;

class Day5 implements Day {
    public int day() {
        return 5;
    }

    private Vector<Vector<Character>> parseStacks(String text) {
        Vector<Vector<Character>> grid = new Vector<>();
        text.lines().forEach(line -> {
            // line.chars().skip(1).stepBy(4).toArray() // NO `stepBy`! :(
            char[] array = line.toCharArray();
            Vector<Character> v = new Vector<>((array.length - 1) / 4);
            for (int i = 1; i < array.length; i += 4) {
                v.add(array[i]);
            }
            grid.add(v);
        });
        int numberCrates = grid.removeLast().size(); // headers 1 2 3 ...
        Vector<Vector<Character>> stacks = new Vector<>(numberCrates);
        for (int r = 0; r < numberCrates; r++) {
            Vector<Character> stack = new Vector<>(grid.size());
            for (int i = grid.size() - 1; i >= 0; i--) {
                char ch = grid.get(i).get(r);
                if (ch == ' ') {
                    break;
                }
                stack.add(ch);
            }
            stacks.add(stack);
        }
        return stacks;
    }

    public String solve(boolean part2, String input) {
        String[] split = input.split("\n\n", 2);
        Vector<Vector<Character>> stacks = parseStacks(split[0]);

        split[1].lines().forEach(line -> {
            String[] split2 = line.split(" ");
            int nb = Integer.parseInt(split2[1]);
            int from = Integer.parseInt(split2[3]) - 1;
            int to = Integer.parseInt(split2[5]) - 1;
            for (int i = 0; i < nb; i++) {
                Vector<Character> stack = stacks.get(from);
                char ch;
                if (!part2) {
                    ch = stack.removeLast();
                } else {
                    ch = stack.remove(stack.size() - nb + i);
                }
                stacks.get(to).add(ch);
            }
        });

        StringBuilder result = new StringBuilder();
        for (Vector<Character> stack : stacks) {
            result.append(stack.lastElement());
        }
        return result.toString();
    }
}
